import 'dart:io';

class taobin {
  int mainmenu = 0;
  String namemenu = "";
  int price = 0;

  taobin(int menu) {
    this.mainmenu = menu;
  }

  String coffee(String HI, int menunumber, int swlevel) {
    if (HI == "h") {
      switch (menunumber) {
        case 1:
          namemenu = "Espresso";
          price = 35;
          break;
        case 2:
          namemenu = "Hot Cafe'Latte";
          price = 35;
          break;
        case 3:
          namemenu = "Hot Matcha Latte";
          price = 35;
          break;
        case 4:
          namemenu = "Hot Mocha";
          price = 35;
          break;
        case 5:
          namemenu = "Hot Cappucino";
          price = 35;
          break;
        case 6:
          namemenu = "Hot Americano";
          price = 35;
          break;
      }
    } else {
      switch (menunumber) {
        case 1:
          namemenu = "Dirty";
          price = 40;
          break;
        case 2:
          namemenu = "Iced Mocha";
          price = 40;
          break;
        case 3:
          namemenu = "Iced Espresso";
          price = 40;
          break;
        case 4:
          namemenu = "Iced Matcha Cafe'Latte";
          price = 40;
          break;
        case 5:
          namemenu = "Iced Cafe' Latte";
          price = 40;
          break;
        case 6:
          namemenu = "Iced Americano";
          price = 40;
          break;
      }
      print("Menu");
      print(namemenu);
      print("SweetLevel");
      print(swlevel);
      print("Price");
      print(price);
      print("smoothie +5 baht");
      print("input y : yes");
      print("input n : no");
      String sm = (stdin.readLineSync()!);
      if (sm == "y") {
        price = price + 5;
      }
    }

    return namemenu;
  }

  String tea(String HI, int menunumber, int swlevel) {
    if (HI == "h") {
      switch (menunumber) {
        case 1:
          namemenu = "Hot Thai Milk Tea";
          price = 35;
          break;
        case 2:
          namemenu = "Hot Taiwanese Tea";
          price = 35;
          break;
        case 3:
          namemenu = "Hot Tea";
          price = 35;
          break;
        case 4:
          namemenu = "Hot Black Tea";
          price = 35;
          break;
        case 5:
          namemenu = "Hot Lime Tea";
          price = 35;
          break;
        case 6:
          namemenu = "Hot Strawberry Tea";
          price = 35;
          break;
      }
    } else {
      switch (menunumber) {
        case 1:
          namemenu = "Iced Thai Milk Tea";
          price = 40;
          break;
        case 2:
          namemenu = "Iced Taiwanese Milk Tea";
          price = 40;
          break;
        case 3:
          namemenu = "Iced Matcha Latte";
          price = 40;
          break;
        case 4:
          namemenu = "Iced Tea";
          price = 40;
          break;
        case 5:
          namemenu = "Iced Lychee Tea";
          price = 40;
          break;
        case 6:
          namemenu = "Iced Blueberry Tea";
          price = 40;
          break;
      }
      print("Menu");
      print(namemenu);
      print("SweetLevel");
      print(swlevel);
      print("Price");
      print(price);
      print("smoothie +5 baht");
      print("input y : yes");
      print("input n : no");
      String sm = (stdin.readLineSync()!);
      if (sm == "y") {
        price = price + 5;
      }
    }

    return namemenu;
  }

  String milk(String HI, int menunumber, int swlevel) {
    if (HI == "h") {
      switch (menunumber) {
        case 1:
          namemenu = "Hot Caramel Milk";
          price = 35;
          break;
        case 2:
          namemenu = "Hot Kokuto Milk";
          price = 35;
          break;
        case 3:
          namemenu = "Hot Cocoa";
          price = 35;
          break;
        case 4:
          namemenu = "Hot Caramel Cocoa";
          price = 35;
          break;
        case 5:
          namemenu = "Hot Milk";
          price = 35;
          break;
      }
    } else {
      switch (menunumber) {
        case 1:
          namemenu = "Iced Caramel Milk";
          price = 40;
          break;
        case 2:
          namemenu = "Iced Kokuto Milk";
          price = 40;
          break;
        case 3:
          namemenu = "Iced Cocoa";
          price = 40;
          break;
        case 4:
          namemenu = "Iced Caramel Cocoa";
          price = 40;
          break;
        case 5:
          namemenu = "Iced Pink Milk";
          price = 40;
          break;
      }
      print("Menu");
      print(namemenu);
      print("SweetLevel");
      print(swlevel);
      print("Price");
      print(price);
      print("smoothie +5 baht");
      print("input y : yes");
      print("input n : no");
      String sm = (stdin.readLineSync()!);
      if (sm == "y") {
        price = price + 5;
      }
    }
    return namemenu;
  }

  String Protein(int menunumber) {
    switch (menunumber) {
      case 1:
        namemenu = "Chocolate Protein Shakes";
        price = 50;
        break;
      case 2:
        namemenu = "Stawberry Protein Shakes";
        price = 50;
        break;
      case 3:
        namemenu = "Caramel Protein Shakes";
        price = 50;
        break;
      case 4:
        namemenu = "Espresso Protein Shakes";
        price = 50;
        break;
      case 5:
        namemenu = "Matcha Protein Shakes";
        price = 50;
        break;
      case 6:
        namemenu = "Taiwanese Tea Protein Shakes";
        price = 50;
        break;
    }
    print("Menu");
    print(namemenu);
    print("Price");
    print(price);
    print("smoothie +5 baht");
    print("input y : yes");
    print("input n : no");
    String sm = (stdin.readLineSync()!);
    if (sm == "y") {
      price = price + 5;
    }
    return namemenu;
  }

  String Soda_Fruity(String SF, int menunumber) {
    if (SF == "s") {
      switch (menunumber) {
        case 1:
          namemenu = "Iced Limenade Soda";
          price = 40;
          break;
        case 2:
          namemenu = "Iced Plum Soda";
          price = 40;
          break;
        case 3:
          namemenu = "Iced Blueberry Soda";
          price = 40;
          break;
        case 4:
          namemenu = "Iced Strawberry Soda";
          price = 40;
          break;
        case 5:
          namemenu = "Pepsi";
          price = 20;
          break;
        case 6:
          namemenu = "Iced Lychee Soda";
          price = 40;
          break;
      }
    } else {
      switch (menunumber) {
        case 1:
          namemenu = "Hot Limeade";
          price = 40;
          break;
        case 2:
          namemenu = "Iced Strawberry";
          price = 40;
          break;
        case 3:
          namemenu = "Iced Blueberry";
          price = 40;
          break;
        case 4:
          namemenu = "Iced Mango";
          price = 40;
          break;
        case 5:
          namemenu = "Iced Plum";
          price = 40;
          break;
        case 6:
          namemenu = "Iced Lychee";
          price = 40;
          break;
      }
    }
    return namemenu;
  }
}

void main() {
  print("TAOBIN");
  print("input 1 = coffee");
  print("input 2 = Tea");
  print("input 3 = milk,cocoa,caramel");
  print("input 4 = protein shake");
  print("input 5 = soda and fruity");
  print("input 6 = cancle");
  print(" Enter your Number ");
  int? menu = int.parse(stdin.readLineSync()!);
  taobin tao = new taobin(menu);
  while (menu != 6) {
    if (menu == 1) {
      print("MENU COFFEE");
      print(
          "--- HOT COFFEE ---                              --- ICED COFFEE ---");
      print(
          "1. Espresso          4. Hot Mocha          ||   1. Dirty             4. Iced Matcha Cafe'Latte"
          "\n"
          "2. Hot Cafe'Latte    5. Hot Cappucino      ||   2. Iced Mocha        5. Iced Cafe' Latte"
          "\n"
          "3. Hot Matcha Latte  6. Hot Americano      ||   3. Iced Espresso     6. Iced Americano"
          "\n");
      print("input h : hot coffee");
      print("input i : iced coffee");
      print("input Menu number");
      String HI = (stdin.readLineSync()!);
      int? menunumber = int.parse(stdin.readLineSync()!);
      print("input sweetLevel");
      print("100 , 70 , 50 , 20 , 0");
      int? sw_level = int.parse(stdin.readLineSync()!);
      tao.coffee(HI, menunumber, sw_level);
      print("Menu");
      print(tao.namemenu);
      print("SweetLevel");
      print(sw_level);
      print("Price");
      print(tao.price);
      print("confirm order and pay");
      print("Thank you for using the service");
    }
    if (menu == 2) {
      print("MENU TEA");
      print(
          "--- HOT TEA ---                                        --- ICED TEA ---");
      print(
          "1. Hot Thai Milk Tea    4. Hot Black Tea          ||   1. Iced Thai Milk Tea         4. Iced Tea"
          "\n"
          "2. Hot Taiwanese Tea    5. Hot Lime Tea           ||   2. Iced Taiwanese Milk Tea    5. Iced Lychee Tea"
          "\n"
          "3. Hot Matcha Latte     6. Hot Strawberry Tea     ||   3. Iced Matcha Latte          6. Iced Blueberry Tea"
          "\n");
      print("input h : hot tea");
      print("input i : iced tea");
      print("input Menu number");
      String HI = (stdin.readLineSync()!);
      int? menunumber = int.parse(stdin.readLineSync()!);
      print("input sweetLevel");
      print("100 , 70 , 50 , 20 , 0");
      int? sw_level = int.parse(stdin.readLineSync()!);
      tao.tea(HI, menunumber, sw_level);
      print("Menu");
      print(tao.namemenu);
      print("SweetLevel");
      print(sw_level);
      print("Price");
      print(tao.price);
      print("confirm order and pay");
      print("Thank you for using the service");
    }
    if (menu == 3) {
      print("MENU MILK,COCOA,CARAMEL");
      print(
          "--- HOT MILK ---                                      --- ICED MILK ---");
      print(
          "1. Hot Caramel Milk      4. Hot Caramel Cocoa    ||   1. Iced Caramel Milk     4. Iced Caramel Cocoa"
          "\n"
          "2. Hot Kokuto Milk       5. Hot Milk             ||   2. Iced Kokuto Milk      5. Iced Pink Milk"
          "\n"
          "3. Hot Cocoa                                     ||   3. Iced Matcha Latte"
          "\n");
      print("input h : hot milk");
      print("input i : iced milk");
      print("input Menu number");
      String HI = (stdin.readLineSync()!);
      int? menunumber = int.parse(stdin.readLineSync()!);
      print("input sweetLevel");
      print("100 , 70 , 50 , 20 , 0");
      int? sw_level = int.parse(stdin.readLineSync()!);
      tao.milk(HI, menunumber, sw_level);
      print("Menu");
      print(tao.namemenu);
      print("SweetLevel");
      print(sw_level);
      print("Price");
      print(tao.price);
      print("confirm order and pay");
      print("Thank you for using the service");
    }
    if (menu == 4) {
      print("MENU PROTEIN SHAKE");
      print("1. Chocolate Protein Shakes        4. Espresso Protein Shakes"
          "\n"
          "2. Stawberry Protein Shakes          5. Matcha Protein Shakes"
          "\n"
          "3. Caramel Protein Shakes            6. Taiwanese Tea Protein Shakes"
          "\n");
      print("input Menu number");
      int? menunumber = int.parse(stdin.readLineSync()!);
      tao.Protein(menunumber);
      print("Menu");
      print(tao.namemenu);
      print("Price");
      print(tao.price);
      print("confirm order and pay");
      print("Thank you for using the service");
    }
    if (menu == 5) {
      print("MENU SODA AND FRUITY");
      print(
          "--- SODA ---                                                   --- FRUITY ---");
      print(
          "1. Iced Limenade Soda      4. Iced Strawberry Soda      ||   1. Hot Limeade          4. Iced Mango"
          "\n"
          "2. Iced Plum Soda          5. Pepsi                     ||   2. Iced Strawberry      5. Iced Plum"
          "\n"
          "3. Iced Blueberry Soda     6. Iced Lychee Soda          ||   3. Iced Matcha Latte    6. Iced Lychee"
          "\n");
      print("input s : soda");
      print("input f : fruity");
      print("input Menu number");
      String SF = (stdin.readLineSync()!);
      int? menunumber = int.parse(stdin.readLineSync()!);
      tao.Soda_Fruity(SF, menunumber);
      print("Menu");
      print(tao.namemenu);
      print("Price");
      print(tao.price);
      print("confirm order and pay");
      print("Thank you for using the service");
    }
    menu = int.parse(stdin.readLineSync()!);
  }
  if (menu == 6) {
    print("Thank you for using the service");
  }
}
